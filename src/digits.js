import { showImage } from './utils.js';

const wait = ms => new Promise(r => setTimeout(r, ms));

const DIGIT_INFERENCE_URL = 'https://digit-inference.azurewebsites.net/api/classify?code=rUmlCWSba0EMe8r2hyajuoav86KwrZqpGeDlvrC5WtgTQmk/tcCoZA==';


function warmupDigitInference() {
    let t0 = localStorage.getItem('lastAzureRequest');
    if (t0 != null && Date.now() - t0 < 10*60*1000) {
        console.log("Azure function is already warm");
        return;
    }

    let payload  = { images: [] };
    fetch(DIGIT_INFERENCE_URL, {
        mode: 'cors',
        method: 'POST',
        body: JSON.stringify(payload),
    }).then(() => {
        console.log("digit inference is ready to go");
        localStorage.setItem('lastAzureRequest', Date.now());
    });
}


async function detectDigits(images) {
    let resized = [];

    for (let i = 0; i < images.length; i++) {
        let dst = new cv.Mat();
        cv.resize(images[i], dst, new cv.Size(28, 28), 0, 0, cv.INTER_AREA);
        resized.push(dst);
    }

    let infer = [];
    let indices = [];

    for (let i = 0; i < resized.length; i++) {
        if (cv.countNonZero(resized[i]) > 30) {
            infer.push(resized[i]);
            indices.push(i);
        }
    }

    let encoded = infer.map(img => btoa(String.fromCharCode(...img.data)));

    let payload = { images: encoded };

    let r = await fetch(DIGIT_INFERENCE_URL, {
        mode: 'cors',
        method: 'POST',
        body: JSON.stringify(payload),
    });


    let body = await r.json();

    let output = Array(images.length).fill({prediction: null});

    for (let i = 0; i < body.length; i++) {
        let idx = indices[i];
        output[idx] = body[i];
    }

    let min_conf = null;

    for (let i = 0; i < body.length; i++) {
        if (min_conf == null || body[i].confidence < min_conf) {
            min_conf = body[i].confidence;
        }
        if (self.debug && (body[i].confidence < 0.9 || body[i].prediction == 0)) {
            postMessage({action: "notify", msg: `prediction: ${body[i].prediction}<br>confidence:${body[i].confidence.toFixed(2)}`, duration: 3000});
            showImage(infer[i]);
            await wait(3200);
        }
    }

    if (min_conf < 0.9) {
        postMessage({action: "notify", msg: `Some digits may not have been detected correcly (lowest confidence: ${min_conf.toFixed(2)})`});
    }

    return output;
}

export { detectDigits, warmupDigitInference };
