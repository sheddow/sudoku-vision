import { isSquare } from './utils.js';


/* eslint-disable no-unused-vars */
const CNT_NEXT = 0;
const CNT_PREV = 1;
const CNT_FIRST_CHILD = 2;
const CNT_PARENT = 3;
/* eslint-enable no-unused-vars */


function Contours(contours, hierarchy) {
    this.contours = contours;
    this.hierarchy = hierarchy;
}

Contours.prototype.biggestContour = function() {
    if (this.contours.size() == 0) {
        return null;
    }

    let biggest_idx = null;
    let biggest_size = null;
    for (let i = 0; i < this.contours.size(); i++) {
        let cnt = this.contours.get(i);
        let s = cv.contourArea(cnt, false);
        if (biggest_size == null || s > biggest_size) {
            biggest_size = s;
            biggest_idx = i;
        }
        cnt.delete();
    }

    return new Contour(this.contours.get(biggest_idx), biggest_idx);
}


Contours.prototype.topLevelChildren = function() {
    return this.getChildren(-1);
}


Contours.prototype.getChildren = function(idx) {
    let children = [];
    for (let i = 0; i < this.hierarchy.data32S.length; i += 4) {
        if (this.hierarchy.data32S[i + CNT_PARENT] == idx) {
            let cnt_idx = Math.floor(i/4);
            children.push({idx: cnt_idx, contour: this.contours.get(cnt_idx)});
        }
    }

    return children.map(({idx, contour}) => new Contour(contour, idx));
}


Contours.prototype.delete = function() {
    for (let i = 0; i < this.contours.size(); i++) {
        this.contours.get(i).delete();
    }
    this.contours.delete();
}


function findContours(img) {
    let contours = new cv.MatVector();
    let hierarchy = new cv.Mat();

    cv.findContours(img, contours, hierarchy, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE);

    return new Contours(contours, hierarchy);
}


function Contour(contour, idx) {
    this.contour = contour;
    this.idx = idx;
}


Contour.prototype.isSquare = function(width, height, options) {
    return isSquare(this.contour, width, height, options);
}


Contour.prototype.area = function() {
    return cv.contourArea(this.contour, false);
}


Contour.prototype.approx = function(alpha=0.1) {
    let approx_cnt = new cv.Mat();
    let perimeter = cv.arcLength(this.contour, true);
    cv.approxPolyDP(this.contour, approx_cnt, alpha*perimeter, true);

    return new Contour(approx_cnt, this.idx);
}

Contour.prototype.delete = function() {
    this.contour.delete();
}

export { Contour, Contours, findContours };
