const wait = ms => new Promise(r => setTimeout(r, ms));


async function processVideo(video) {
    let width = video.width;
    let height = video.height;

    let cap = new cv.VideoCapture(video);
    let worker = new Worker(new URL("./worker.js", import.meta.url));
    window.worker = worker;

    function sendNextFrame() {
        let src = new cv.Mat(height, width, cv.CV_8UC4);
        cap.read(src);

        let buf = typedArrayToBuffer(src.data);
        worker.postMessage({
            rows: src.rows,
            cols: src.cols,
            type: src.type(),
            data: buf,
        }, [buf]);

        src.delete();
    }

    let loader = document.getElementById("loader");
    let load_msg = document.getElementById("load_msg");
    let initial_loader = document.getElementById("initial_loader");
    let canvas = document.getElementById("canvasOutput");
    let videoElem = document.getElementById("video");
    let notification = document.getElementById("notification");

    notification.onclick = function() {
        notification.classList.add("hidden");
    };

    worker.onmessage = async function(e) {
        if (e.data.action == "next_frame") {
            await wait(25); // avoid 100% cpu usage in worker
            sendNextFrame();
        }
        else if (e.data.action == "alert") {
            alert(e.data.msg);
        }
        else if (e.data.action == "show_image") {
            videoElem.classList.add("hidden");
            canvas.classList.remove("hidden");
            let img = cv.matFromArray(e.data.rows, e.data.cols, e.data.type, new Uint8Array(e.data.data));
            cv.imshow("canvasOutput", img);
        }
        else if (e.data.action == "loading") {
            loader.classList.remove("hidden");
            load_msg.innerHTML = e.data.msg;
        }
        else if (e.data.action == "stop_loading") {
            loader.classList.add("hidden");
        }
        else if (e.data.action == "start_video") {
            initial_loader.classList.add("hidden");
            if (location.search.indexOf("debug") > -1) {
                canvas.classList.remove("hidden");
                worker.postMessage({action: "set_debug"});
                window.debug = true;
            }
            else {
                videoElem.classList.remove("hidden");
            }
            sendNextFrame();
        }
        else if (e.data.action == "notify") {
            let duration = e.data.duration || 5000;
            notification.innerHTML = e.data.msg;
            notification.classList.remove("hidden");
            await wait(duration);
            notification.classList.add("hidden");
        }
        else if (e.data.action == "reload") {
            worker.terminate();
            videoElem.classList.add("hidden");
            canvas.classList.add("hidden");
            initial_loader.classList.remove("hidden");
            processVideo(video);
        }
        else if (e.data.action == "error") {
            console.log(e.data.error);
            let msg = e.data.error.stack || e.data.error;
            alert(`worker threw error: ${msg}`);
        }
        else {
            alert("error: unknown message action: " + e.data.action);
        }
    }
}

function sendTestFrame() {
    let src = cv.imread("test_img");

    let buf = typedArrayToBuffer(src.data);
    worker.postMessage({
        rows: src.rows,
        cols: src.cols,
        type: src.type(),
        data: buf,
    }, [buf]);

    src.delete();
}
window.sendTestFrame = sendTestFrame;


function typedArrayToBuffer(array) {
    return array.buffer.slice(array.byteOffset, array.byteOffset + array.byteLength)
}


export { processVideo };
