function sortBy(l, f) {
    l.sort((a, b) => f(a) - f(b));
}


function showImage(img) {
    let data = img.data;
    let buf = data.buffer.slice(data.byteOffset, data.byteOffset + data.byteLength);

    postMessage({
        action: "show_image",
        rows: img.rows,
        cols: img.cols,
        type: img.type(),
        data: buf
    }, [buf]);
}

function magnitude(x, y) {
    return Math.sqrt(x**2 + y**2)
}

// cosine of angle between two vectors using dot product
function cosine(x1, y1, x2, y2) {
    return (x1*x2 + y1*y2)/(magnitude(x1, y1)*magnitude(x2, y2))
}

function isSquare(contour, width, height, {angle_eps=0.1, check_horizontal=false}={}) {
    let size = Math.max(width, height);
    if (!contour.data32S.length == 8) {
        return false;
    }

    if (check_horizontal) {
        let x1 = contour.data32S[0];
        let y1 = contour.data32S[1];

        let x2 = contour.data32S[2];
        let y2 = contour.data32S[3];

        // line can be horizontal or vertical
        if (!(almostEqual(x1, x2, 0.1*size) || almostEqual(y1, y2, 0.1*size))) {
            return false;
        }
    }

    let side_lengths = [];
    for (let i = 0; i < 8; i += 2) {
        let x1 = contour.data32S[i];
        let y1 = contour.data32S[i+1];

        let x2 = contour.data32S[(i+2) % 8];
        let y2 = contour.data32S[(i+3) % 8];

        let x3 = contour.data32S[(i+4) % 8];
        let y3 = contour.data32S[(i+5) % 8];

        let v1x = x1 - x2;
        let v1y = y1 - y2;

        let v2x = x3 - x2;
        let v2y = y3 - y2;

        if (Math.abs(cosine(v1x, v1y, v2x, v2y)) > angle_eps) {
            return false;
        }

        side_lengths.push(magnitude(v1x, v1y));
    }
    let avg = side_lengths.reduce((a, b) => a+b, 0) / 4;

    for (let side_len of side_lengths) {
        if (!almostEqual(side_len, avg, 0.1*size)) {
            return false;
        }
    }


    return true;
}

function almostEqual(a, b, diff) {
    return Math.abs(a - b) < diff;
}


export { showImage, isSquare, sortBy };
