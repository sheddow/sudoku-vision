import { processVideo } from "./video.js";
import { warmupDigitInference } from "./digits.js";


async function startVideo() {
    if (!('mediaDevices' in navigator && 'getUserMedia' in navigator.mediaDevices)) {
        alert("missing media functionality");
        return;
    }

    let doc = document.documentElement;
    let w = doc.clientWidth;
    let h = doc.clientHeight;

    let stream;
    try {
        stream = await navigator.mediaDevices.getUserMedia({video: {facingMode: "environment"}});
    }
    catch (e) {
        alert("denied media permissions");
        throw e;
    }

    let canvas = document.getElementById('canvasOutput');
    canvas.style.width = `${w}px`;
    canvas.style.height = `${h}px`;

    let video = document.getElementById('video');

    video.srcObject = stream;
    video.onloadedmetadata = function() {
        video.play();

        video.width = video.videoWidth;
        video.height = video.videoHeight;

        processVideo(video);
    }
}

window.onload = function() {
    if (location.protocol !== 'https:') {
        location.replace(`https:${location.href.substring(location.protocol.length)}`);
        return;
    }
    warmupDigitInference();

    let script = document.createElement("script");
    script.onload = async function () {

        console.log("compiling opencv...");
        try {
            window.cv = await cv;
        }
        catch (e) {
            alert(`error when compiling OpenCV: ${e}`);
            return;
        }
        console.log("opencv is ready");
        startVideo();
    }

    script.src = "opencv.js";

    document.head.appendChild(script);
}
