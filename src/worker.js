import { detectDigits } from "./digits.js";
import { showImage, sortBy } from "./utils.js";
import { findContours } from "./contours.js";


/* eslint-disable no-unused-vars */
const CC_LEFT = 0;
const CC_TOP = 1;
const CC_WIDTH = 2;
const CC_HEIGHT = 3;
const CC_AREA = 4;
/* eslint-enable no-unused-vars */


self.debug = false;


function normalize_brightness(src, dst) {
    let background = new cv.Mat();

    let M = cv.getStructuringElement(cv.MORPH_ELLIPSE, {width: 5, height: 5});
    cv.morphologyEx(src, background, cv.MORPH_CLOSE, M);

    let a = new cv.Mat();
    src.convertTo(a, cv.CV_32F);

    let b = new cv.Mat();
    background.convertTo(b, cv.CV_32F);

    let tmp = new cv.Mat();

    cv.divide(a, b, tmp);

    cv.normalize(tmp, tmp, 0, 255, cv.NORM_MINMAX);

    tmp.convertTo(dst, src.type());

    background.delete();
    M.delete();
    a.delete();
    b.delete();
    tmp.delete();
}


function threshold(src, dst) {
    let tmp = new cv.Mat();

    cv.cvtColor(src, tmp, cv.COLOR_RGBA2GRAY);

    normalize_brightness(tmp, tmp);

    // randomized between 215 and 240, skewed to higher values
    let threshold_level = 215 + Math.pow(Math.random(), 0.5)*25;

    cv.threshold(tmp, dst, threshold_level, 255, cv.THRESH_BINARY);

    cv.bitwise_not(dst, dst);

    tmp.delete();
}


// returns points in order
// [top-left, top-right, bottom-left, bottom-right]
function orderedPoints(contour) {
    let m = [];
    for (let i = 0; i < 8; i += 2) {
        m.push([contour.data32S[i], contour.data32S[i+1]]);
    }

    let points = Array(4);
    sortBy(m, p => p[0]+p[1]);

    points[0] = m[0];
    points[3] = m[3];

    sortBy(m, p => p[1]-p[0]);

    points[1] = m[0];
    points[2] = m[3];

    return points;
}

function pointsToContour(points) {
    return cv.matFromArray(4, 1, cv.CV_32FC2, points.flat());
}

function warpToContour(img, contour, n=10) {
    let points = orderedPoints(contour);

    // expand contour on all sides
    points[0][0] -= n;
    points[0][1] -= n;
    points[1][0] += n;
    points[1][1] -= n;
    points[2][0] -= n;
    points[2][1] += n;
    points[3][0] += n;
    points[3][1] += n;

    let width = Math.max(points[1][0] - points[0][0],
                         points[3][0] - points[2][0]);
    let height = Math.max(points[2][1] - points[0][1],
                          points[3][1] - points[1][1]);

    let source = pointsToContour(points);
    let target = pointsToContour([[0, 0], [width, 0], [0, height], [width, height]]);

    let M = cv.getPerspectiveTransform(source, target);
    let res = new cv.Mat();
    cv.warpPerspective(img, res, M, {width, height});
    M.delete();

    return res;
}


function cleanImage(img, width, height) {
    // zero out borders
    for (let r = 0; r < img.rows; r++) {
        for (let c = 0; c < img.cols; c++) {
            let pixel = img.ucharPtr(r, c);
            if (r < 0.1*img.rows || r > 0.9*img.rows ||
                c < 0.1*img.cols || c > 0.9*img.cols) {
                pixel[0] = 0;
            }
        }
    }

    let labels = new cv.Mat();
    let stats = new cv.Mat();
    let centroids = new cv.Mat();

    cv.connectedComponentsWithStats(img, labels, stats, centroids);

    // zero out noise
    for (let r = 0; r < img.rows; r++) {
        for (let c = 0; c < img.cols; c++) {
            let pixel = img.ucharPtr(r, c);
            let label = labels.intPtr(r, c)[0];

            let area = stats.data32S[5*label + CC_AREA];

            if (area < 0.05*width*height) {
                pixel[0] = 0;
            }
        }
    }
    labels.delete();
    stats.delete();
    centroids.delete();
}

onmessage = function(e) {
    if (e.data.action == "set_debug") {
        self.debug = true;
        return;
    }

    messageHandler(e).catch(err => {
        if (typeof err == "number") {
            err = cv.exceptionFromPtr(err).msg;
        }
        postMessage({action: "error", error: err});
    });
}

async function messageHandler(e) {
    let height = e.data.rows;
    let width = e.data.cols;

    let src = cv.matFromArray(height, width, e.data.type, new Uint8Array(e.data.data));

    // threshold
    // find biggest contour
    // if almost square and big enough:
    // do perspective transform to yield a perfect square
    // find contours again on square
    // there should be 1, 9 or 81 top-level contours
    // if 1, get its children
    // if 9, divide each into 9
    // check each cell for squareness and equal size
    // clean each cell using connected components

    let thresh = new cv.Mat(height, width, cv.CV_8UC1);
    threshold(src, thresh);

    if (self.debug) {
        showImage(thresh);
    }

    let contours = findContours(thresh);
    let cnt = contours.biggestContour();

    if (cnt == null) {
        postMessage({action: "next_frame"});
        src.delete();
        thresh.delete();
        contours.delete();
        return;
    }

    cnt = cnt.approx();

    let area = cnt.area();
    if (!((area > 0.25*width*height || area > 300*300) && cnt.isSquare(width, height))) {
        postMessage({action: "next_frame"});
        src.delete();
        thresh.delete();
        contours.delete();
        cnt.delete();
        return;
    }

    postMessage({action: "loading", msg: "analyzing square"});

    let square_thresh = warpToContour(thresh, cnt.contour);
    let square_img = warpToContour(src, cnt.contour);

    // redo contour analysis on warped image
    contours = findContours(square_thresh);

    let children = contours.topLevelChildren();
    let children_ = [];
    for (let c of children) {
        if (c.area() > 0.7*square_img.rows*square_img.cols) {
            children_.push(c);
        }
    }
    children = children_;

    if (children.length == 1) {
        children = contours.getChildren(children[0].idx);
    }

    if (children.length != 9 && children.length != 81) {
        postMessage({action: "stop_loading"});
        postMessage({action: "next_frame"});
        src.delete();
        thresh.delete();
        square_img.delete();
        square_thresh.delete();
        contours.delete();
        cnt.delete();
        return;
    }

    // check squareness and equal size of cells
    let areas = [];
    for (let c of children) {
        let cnt = c.approx();
        if (!cnt.isSquare(square_img.cols, square_img.rows, {check_horizontal: true})) {
            postMessage({action: "stop_loading"});
            postMessage({action: "next_frame"});
            src.delete();
            thresh.delete();
            square_img.delete();
            square_thresh.delete();
            contours.delete();
            cnt.delete();
            return;
        }
        areas.push(cnt.area());
        cnt.delete();
    }
    let mean_area = areas.reduce((a, b) => a + b)/areas.length;
    for (let a of areas) {
        if (Math.abs(a - mean_area) > square_img.rows*square_img.cols/100) {
            postMessage({action: "stop_loading"});
            postMessage({action: "next_frame"});
            src.delete();
            thresh.delete();
            square_img.delete();
            square_thresh.delete();
            contours.delete();
            cnt.delete();
            return;
        }
    }


    let unsorted_cells = [];
    if (children.length == 9) {
        // divide each into 9 equal squares
        for (let cnt of children) {
            let {x: x0, y: y0, width, height} = cv.boundingRect(cnt.contour);
            for (let i = 0; i < 3; i++) {
                for (let j = 0; j < 3; j++) {
                    let x = x0 + Math.floor((width/3)*i);
                    let y = y0 + Math.floor((height/3)*j);

                    let rect = {x, y, width: width/3, height: height/3};
                    let img = square_thresh.roi(rect);

                    let cx = x + Math.floor(width/6);
                    let cy = y + Math.floor(height/6);

                    unsorted_cells.push({img, x: cx, y: cy});
                }
            }
        }
    }
    else if (children.length == 81) {
        for (let cnt of children) {
            let rect = cv.boundingRect(cnt.contour);
            let img = square_thresh.roi(rect);

            let M = cv.moments(cnt.contour);
            let cx = M.m10/M.m00;
            let cy = M.m01/M.m00;

            unsorted_cells.push({img, x: cx, y: cy});
        }
    }

    if (self.debug) {
        showImage(square_thresh);
    }
    else {
        showImage(square_img);
    }

    postMessage({action: "loading", msg: "analyzing cells"});

    // sort vertically
    unsorted_cells.sort((a, b) => a.y - b.y);

    // sort horizontally
    let cells = []
    for (let i = 0; i < 9; i++) {
        let row = unsorted_cells.slice(i*9, (i+1)*9);
        sortBy(row, c => c.x);

        cells.push(...row);
    }

    for (let cell of cells) {
        cleanImage(cell.img, cell.cols, cell.rows);
    }

    postMessage({action: "loading", msg: "detecting digits"});

    let digits = await detectDigits(cells.map(c => c.img));
    digits = digits.map(d => d.prediction);


    postMessage({action: "loading", msg: "calculating solution"});

    let inp = digits.map(d => d || 0);
    let solver = await import("../pkg/index.js");

    if (!solver.is_consistent(inp)) {
        postMessage({action: "stop_loading"});
        postMessage({action: "alert", msg: "sudoku is invalid (this can happen if digits were incorrectly detected)"});
        postMessage({action: "reload"});
        return;
    }

    let solution = solver.solve_sudoku(inp);

    if (!solution) {
        postMessage({action: "stop_loading"});
        postMessage({action: "alert", msg: "sudoku seems to be unsolvable (this can happen if digits were incorrectly detected)"});
        postMessage({action: "reload"});
        return;
    }

    for (let i = 0; i < cells.length; i++) {
        let {x, y} = cells[i];

        if (digits[i] == null) {
            cv.putText(square_img, solution[i].toString(), {x: x - 9, y: y + 9}, cv.FONT_HERSHEY_SIMPLEX, 1, [0, 0, 0, 255]);
        }
    }

    postMessage({action: "stop_loading"});
    showImage(square_img);

    src.delete();
    thresh.delete();
    square_img.delete();
    square_thresh.delete();
    contours.delete();
    cnt.delete();
}


importScripts("opencv.js");

cv().then(cv => {
    self.cv = cv;
    postMessage({action: "start_video"});
});
