const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");
const WasmPackPlugin = require("@wasm-tool/wasm-pack-plugin");


module.exports = {
    experiments: {
        syncWebAssembly: true
    },
    mode: 'production',
    resolve: {
        fallback: {
            fs: false,
            path: false,
            crypto: false,
        }
    },
    entry: './src/index.js',
    devServer: {
        contentBase: './dist',
        host: '0.0.0.0',
        port: 9000,
        https: true,
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "Sudoku vision",
            scriptLoading: 'defer',
            template: 'public/index.html',
        }),
        new CopyPlugin([
            path.resolve(__dirname, "static")
        ]),
        new WasmPackPlugin({
            crateDirectory: __dirname,
        }),
    ],
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },
};
