# Sudoku Vision
## Try it!
Visit <https://sheddow.xyz/sudoku-vision> (ideally on a phone) and point the camera at a sudoku. If the sudoku is on a warped surface the application might not work. Currently you need to refresh the page if you want to solve another sudoku. Visit <https://sheddow.xyz/sudoku-vision?debug> for a debugging mode.

## How it works
Sudoku Vision uses OpenCV to do thresholding and contour analysis on frames to find a square that could potentially be a sudoku grid. If the square looks like a sudoku grid, each non-empty cell is sent to an Azure Functions instance for digit detection (see <https://gitlab.com/sheddow/azure-digit-inference>). The sudoku is then solved using constraint propagation and depth-first search, implemented in Rust and compiled to WebAssembly.

## Setup

### Build opencv.js (optional)
1. Clone <https://github.com/opencv/opencv>
2. Remove pthread compiler option in cmake/OpenCVCompilerOptions.cmake (because of <https://github.com/opencv/opencv/issues/19243>)
3. Modify platforms/js/opencv_js.config.py to include only the core and imgproc functions
4. Setup emscripten (see https://emscripten.org/docs/getting_started/downloads.html)
5. `source ./emsdk_env.sh` and set `EMSCRIPTEN` variable
6. run `python build_js.py --build_wasm build_wasm --config opencv_js.config.py --enable_exception` in opencv/platforms/js directory

### Build
Run `npm install`, then `npm run build`

## TODO
- improve digit detection
- install tailwind properly - or use custom css
- add option to try again after showing solution
- don't analyse blurry frames (see <https://stackoverflow.com/questions/28717054/calculating-sharpness-of-an-image>)
